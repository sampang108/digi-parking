import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import MapView from 'react-native-maps';
import { Provider } from 'react-redux';
import { persistor, store } from './src/store';
import { PersistGate } from 'redux-persist/lib/integration/react';
import './ReactotronConfig';
import Home from "./src/containers/Home";
import RootNavigator from './src/routers';

export default class App extends React.Component {
  render() {
    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <RootNavigator/>
            </PersistGate>
        </Provider>
    );
  }
}
