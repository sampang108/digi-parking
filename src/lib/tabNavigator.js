import { createStackNavigator } from 'react-navigation';
import React from 'react';
import mapNavigationStateParamsToProps from './navigation';
import {StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class TabOptions {
    static Screen(Component, title = 'Default Title', icon = 'home', screens = []) {
        const GetScreens = () => {
            let screen = { [`${title}`] : {screen: mapNavigationStateParamsToProps(Component)} };
            if (screens.length > 0) {
                screens.forEach(route => {
                    screen[route.name] = {screen: mapNavigationStateParamsToProps(route.screen)}
                });
            }
            return screen;
        };

        return({
            screen: createStackNavigator({
                ...GetScreens()
            }, {
                initialRoute: `${title}`,
                headerMode: 'none'
            }),
            navigationOptions: {
                tabBarIcon: ({ focused, tintColor }) => {
                    // You can return any component that you like here! We usually use an
                    // icon component from react-native-vector-icons
                    return <Icon name={icon} size={25}/>;
                },
            },
            tabBarIcon: <Icon name={icon} size={25}/>,
            tabBarOptions: {
                activeTintColor: 'tomato',
                inactiveTintColor: 'gray',
            },
        })

    }


}