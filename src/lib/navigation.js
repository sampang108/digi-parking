import React from 'react';
// import { storeActionProps } from '../routes'

const mapNavigationStateParamsToProps = (SomeComponent, header = {}) => {
    return class extends React.Component {
        static navigationOptions =  SomeComponent.navigationOptions; // better use hoist-non-react-statics
        render() {
            const {navigation: {state: {params}}} = this.props;
            return <SomeComponent {...params} {...this.props} />
        }
    }
};

export default mapNavigationStateParamsToProps;