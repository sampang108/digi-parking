import {store} from '../store';

class Api {
    static headers() {
        return {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'dataType': 'json',
            'Authorization': `Bearer ${store.getState().user.token}`
        }
    }

    static get(route) {
        console.log('get got in');
        return this.xhr(route, null, 'GET');
    }

    static put(route, params) {
        return this.xhr(route, params, 'PUT')
    }

    static post(route, params) {
        return this.xhr(route, params, 'POST')
    }

    static delete(route, params) {
        return this.xhr(route, params, 'DELETE')
    }

    static xhr(route, params, verb) {
        const host = 'https://jobajoba.selise.org/api/v1';
        const url = `${host}${route}`;
        let options = Object.assign({method: verb}, params ? {body: JSON.stringify(params)} : null);
        options.headers = Api.headers();
        return fetch(url, options).then(resp => {
            let json = resp.json();
            if (resp.ok) {
                return json
            }
            throw(json);
        }, (err) => console.log(err))
    }
}
export default Api