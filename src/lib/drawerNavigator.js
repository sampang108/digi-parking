import { createStackNavigator } from 'react-navigation';
import React from 'react';
import mapNavigationStateParamsToProps from './navigation';
import { StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {NavigationEvents} from 'react-navigation';

const BackButton = props => {
    return(
        <TouchableOpacity style={style.menu} onPress={() => navigation.goBack()}>
            <Icon name="caret-left" size={25} />
        </TouchableOpacity>
    )
};

export default class DrawerOptions {
    static Screen(Component, title = 'Default Title', icon = 'home', screens = []) {
        const style = StyleSheet.create({
                menu: {
                    paddingHorizontal: 15,
                    paddingVertical: 15
                }
        });

        const GetScreens = () => {
            // Component.navigationOptions = ({navigation}) => {
            //     return {
            //         headerLeft: <TouchableOpacity style={style.menu} onPress={() => navigation.toggleDrawer()}>
            //             <Icon name="bars" size={25} />
            //         </TouchableOpacity>,
            //         ...Component.navigationOptions
            //     }
            // };
            let screen = { [`${title}`] : {screen: mapNavigationStateParamsToProps(Component)} };
            if (screens.length > 0) {
                screens.forEach(route => {
                    screen[route.name] = {screen: mapNavigationStateParamsToProps(route.screen)}
                });
            }
            return screen;
        };

        return ({
            screen: createStackNavigator({
                ...GetScreens()
            }, {
                initialRoute: `${title}`,
                navigationOptions: ({navigation}) => {
                    return {
                        title: title,
                        headerStyle: {
                            backgroundColor: '#043B9C',
                            height: 56
                        },
                        headerLeft: <TouchableOpacity style={style.menu} onPress={() => navigation.toggleDrawer()}>
                            <Icon name="bars" size={25} />
                        </TouchableOpacity>,
                        headerTintColor: '#fff',
                        headerTitleStyle: {
                            fontWeight: 'bold',
                        },
                    }
                }
            }), navigationOptions: {
                drawerIcon: <Icon name={icon} size={20}/>
            }
        })

    }


}