import React from 'react';
import { Stylesheet } from 'react-native';

const primaryColor = '#665EFF';
const secondaryColor = '#5773FF';
const infoColor = '#3497FD';
const accentColor = '#3ACCE1';
export const Styles = StyleSheet.create({
    primaryStyles: {
        color: primaryColor,
        backgroundColor: primaryColor,
        borderColor: primaryColor
    },
    secondaryStyles: {
        color: secondaryColor,
        backgroundColor: secondaryColor,
        borderColor: secondaryColor
    },
    infoStyles: {
        color: infoColor,
        backgroundColor: infoColor,
        borderColor: infoColor
    },
    accentStyles: {
        color: accentColor,
        backgroundColor: accentColor,
        borderColor: accentColor
    }
});