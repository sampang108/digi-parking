import {StyleSheet, View, Text} from "react-native";
import React, { Component } from 'react';

export default class Login extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <View style={styles.container}>
               <Text>Login Page</Text>
            </View>

        )
    }
}


const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    card: {
        position: 'absolute',
        height: '30%',
        width: '100%',
        bottom: 5,
        left: 4,
        right: 8,
    },
});
