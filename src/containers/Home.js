import { Button, StyleSheet, Text, View } from 'react-native';
import React, { Component } from 'react';
import Login from './Login';
import { Icon, SocialIcon } from 'react-native-elements';

export default class Home extends Component {
    constructor(props) {
        super(props);
    }

    navigateTo() {
        this.props.navigation.navigate('Login');
    }

    render() {
        return (
            <View style={styles.container}>
                <Icon
                    name='rowing' />

                <Icon
                    name='g-translate'
                    color='#00aced' />
                <Icon
                    raised
                    name='heartbeat'
                    type='font-awesome'
                    color='#f50'
                    onPress={() => console.log('hello')} />
                <SocialIcon
                    type='twitter'
                />
                <SocialIcon
                    button
                    type='instagram'
                />
                <Button style={styles.button} title="Login" onPress={() => this.navigateTo()}/>
                {/*<MapView*/}
                {/*style={styles.map}*/}
                {/*region={{*/}
                {/*latitude: 27.4277805,*/}
                {/*longitude: 89.6487995,*/}
                {/*latitudeDelta: 0.0092,*/}
                {/*longitudeDelta: 0.0042*/}
                {/*}}>*/}
                {/*<MapView.Marker*/}
                {/*coordinate={{*/}
                {/*latitude: 27.4277805,*/}
                {/*longitude: 89.6487995*/}
                {/*}}*/}
                {/*title={'Thimphu Tech Park'}*/}
                {/*description={'my marker description'}*/}
                {/*/>*/}

                {/*</MapView>*/}

            </View>

        )
    }
}


const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        alignItems: 'center',
    },
    map: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    card: {
        position: 'absolute',
        height: '30%',
        width: '100%',
        bottom: 5,
        left: 4,
        right: 8,
    },
    hi: {
        position: 'absolute',
        top: 20
    },
    button: {
        position: 'absolute',
        top: 40
    }
});
