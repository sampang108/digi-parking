export class User {
    isLogin = false;
    username = null;
    token = null;
    isHydrated = false;
    loggedIn_at = null;
    name = null;
    phone = null;
    id = null;
}