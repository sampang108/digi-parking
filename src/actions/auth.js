import * as types from './types';
import API from '../lib/api';
import NavigationService from '../services/NavigationService';


export const register = (user) => {
    return (dispatch, getState) => {
        return API.post('/users/register', user).then(response => {
            NavigationService.navigate('Login');
        });
    }
};

// login
export const login = (user) => {
    return (dispatch, getState) => {
        return API.post('/users/sign_in', user).then(user => {
             dispatch(setUser(user));
        }).catch(ex => console.log(ex));
    }
};

export const setUser = (user) => {
    return {
        type: types.SET_CURRENT_USER,
        user
    }
};

export const passwordReset = (user, forgot = true) => {
    const api = forgot ? 'post' : 'put';
    return (dispatch, getState) => {
        return API[api]('users/password', user).then(response => {
            NavigationService.navigate('Login');
        });
    }
};
