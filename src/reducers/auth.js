import createReducer from "../lib/createReducers";
import Models from "../models";
import * as types from "../actions/types";

export const user = createReducer(new Models.User(), {
    [types.SET_CURRENT_USER](state, action){
        console.log(action);
        const newState = Object.assign(new Models.User(), action.user.user);
        return newState;
    },
    [types.LOGOUT_USER](state, action) {
        return new Models.User();
    }
});