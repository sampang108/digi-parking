import {createStore, applyMiddleware, compose } from 'redux';
import {createLogger} from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import reducer from './reducers';


import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';


const persistConfig = {
    key: 'root',
    storage: storage,
    whitelist: ['user']
};

const pReducer = persistReducer(persistConfig, reducer);


const loggerMiddleWare = createLogger({});

const configureStore = (initialState) => {
    const enhancer = compose(
        applyMiddleware(
            thunkMiddleware,
            loggerMiddleWare
        )
    );
    return createStore(pReducer, enhancer);
};

export const store = configureStore({});
export const persistor = persistStore(store);
