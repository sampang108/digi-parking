import { createDrawerNavigator, createBottomTabNavigator } from 'react-navigation';
import Home from '../containers/Home';
import Login from '../containers/Login';
import { Icon } from 'react-native-elements'


import DrawerOptions from '../lib/drawerNavigator';

const RootNavigator = createDrawerNavigator({
    Home: DrawerOptions.Screen(Home, 'Home'),
    Login: DrawerOptions.Screen(Login, 'Login', 'rowing'),
});

export default RootNavigator;
